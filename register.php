<?php
require "db_functions.php";
include "sanitize.php";

$error = false;
$success = false;
$name = $email = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["confirm_password"])) {

    $conn = connect_db();

    $name = mysqli_real_escape_string($conn,$_POST["name"]);
    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $confirm_password = mysqli_real_escape_string($conn,$_POST["confirm_password"]);
    $name = sanitize($name);
    $email = sanitize($email);
    $password = sanitize($password);
    $confirm_password = sanitize($confirm_password);

    if (filter_var($email, FILTER_VALIDATE_EMAIL)){
      if ($password == $confirm_password) {
        $password = md5($password);

        $sql = "INSERT INTO $table_users
                (name, email, password) VALUES
                ('$name', '$email', '$password');";

        if(mysqli_query($conn, $sql)){
          $success = true;
        }
        else {
          $error_msg = mysqli_error($conn);
          $error = true;
        }
      }
      else {
        $error_msg = "Senha não confere com a confirmação.";
        $error = true;
      }
    } 
    else {
      $error_msg = "Formato de email inválido";
      $error = true;
    }  
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="stylesheet.css">
  <title>Registro - Blog do Senji</title>
</head>
<body>
  <div class="container">
    <h1>Dados para registro de novo usuário</h1><br>

    <?php if ($success): ?>
      <h3 style="color:lightgreen;">Usuário criado com sucesso!</h3>
    <?php endif; ?>

    <?php if ($error): ?>
      <h3 style="color:red;"><?php echo $error_msg; ?></h3>
    <?php endif; ?>

    <form action="register.php" method="post" class="form-horizontal">
      <div class="form-group">  
        <label class="control-label col-sm-3" for="name">Nome: </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" required><br>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="email">Email: </label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" required><br>
        </div>
       </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="password">Senha: </label>
        <div class="col-sm-9">
          <input type="password" class="form-control" name="password" value="" required><br>
        </div>  
      </div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="confirm_password">Confirmação da Senha: </label>
        <div class="col-sm-9">
          <input type="password" class="form-control" name="confirm_password" value="" required><br>
        </div>  
      </div>
      <div class="botoes">
        <input class="btn btn-default" type="submit" name="submit" value="Criar usuário">
        <a class="btn btn-default" href="index.php">Voltar</a></li>
      </div>
    </form>
    
  </div>
</body>
</html>
