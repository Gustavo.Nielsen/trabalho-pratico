<?php
  require "authenticate.php";
  require_once "db_credentials.php";
  include "sanitize.php";
    
  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }

    $form_name = $form_comment = $form_artigoID = "";
    $msg = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if(isset($_POST['form_comment']) && isset($_POST['form_artigoID']) && ($_POST['form_comment'] <> "")){
        $form_name = $_POST['form_name'];
        $form_comment = $_POST['form_comment'];
        $form_artigoID = $_POST['form_artigoID'];

        $name = mysqli_real_escape_string($conn, $form_name);
        $comment = mysqli_real_escape_string($conn, $form_comment);
        $artigoID = mysqli_real_escape_string($conn, $form_artigoID);
        $name = sanitize($name);
        $comment = sanitize($comment);
        $artigoID = sanitize($artigoID);

        $sql = "INSERT INTO $table_comments (nome, comentario, artigoID)
          VALUES ('$name', '$comment', '$artigoID')";

        if (!mysqli_query($conn, $sql)) {
          die("Error: " . $sql . "<br>" . mysqli_error($conn));
        }
        else {
          $form_name = $form_comment = $form_artigoID = "";
          $msg = "Comentário salvo com sucesso!". $artigoID;
        }
      }	
      elseif(isset($_POST["novo-comentario"]) && isset($_POST["id"]) && isset($_POST["artigoID"])){

        $novo_comentario = $_POST["novo-comentario"];
        $novo_comentario = sanitize($novo_comentario);
        $id = $_POST["id"];
        $id = sanitize($id);
        $artigoID = $_POST["artigoID"];
        $artigoID = sanitize($artigoID);
      
        $sql = "UPDATE $table_comments
            SET comentario='". mysqli_real_escape_string($conn, $novo_comentario) .
            "' WHERE id=" . mysqli_real_escape_string($conn, $id);

        if(!mysqli_query($conn,$sql)){
          die("Problemas para executar ação no BD!<br>".
            mysqli_error($conn));
        } else {
          header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php#form-anchor" . $artigoID . "comment");
          exit();
        }
      }
    }
    elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {
      if (isset($_GET["acao"]) && isset($_GET["id"])) {	
        $sql = "";
        $id = $_GET['id'];
        $id = mysqli_real_escape_string($conn, $id);
        $id = sanitize($id);
        if($_GET["acao"] == "remove"){
          $sql = "DELETE FROM $table_comments
              WHERE id=" . $id;
        }
        if($_GET["acao"] == "removetxt"){
          $sql = "DELETE FROM $table_articles
              WHERE id=" . $id;
        }
        if ($sql != "") {
          if(!mysqli_query($conn,$sql)){
            die("Problemas para executar ação no BD!<br>".
              mysqli_error($conn));
          }
        }
      }
    }
    $sql = "SELECT * FROM $table_comments";
    $comments = mysqli_query($conn, $sql);

    if (!$comments) {
      die("Error: " . $sql . "<br>" . mysqli_error($conn));
    }

    $sql = "SELECT * FROM $table_articles";
    $textos = mysqli_query($conn, $sql);

    if (!$textos) {
      die("Error: " . $sql . "<br>" . mysqli_error($conn));
    }

    mysqli_close($conn);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<link rel="stylesheet" href="stylesheet.css">
    <title>Blog do Senji</title>
	</head>
	<body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Blog do Senji</a>
        </div>
        <ul class="nav navbar-nav">
          <li class="active"><a href="index.php">Home</a></li>
          <?php if ($login): ?>
            <li><a href="logout.php">Logout</a></li>
            <li><a href="register.php">Registrar-se</a></li>
          <?php else: ?>
            <li><a href="login.php">Login</a></li>
          <?php endif; ?>
        </ul>
      </div>
    </nav>
		
    <div class="container">
      <div id="texts">
        <div class="text-center">
          <h1>ARTIGOS<h1>
        </div>
        <?php if (mysqli_num_rows($textos) > 0): ?>
          <?php while($texto = mysqli_fetch_assoc($textos)): ?>
            <hr>
            <div class="container" id="texto_<?= $texto['id'] ?>">
              <a name="form-anchor<?php echo $texto['id']; ?>texto"></a>
              <h1 class="titulo"><?= $texto['titulo'] ?></h1>
              <p><?= $texto['texto'] ?></p>
              <?php if($login): ?>
                <a class="btn btn-default btn-xs" href="<?php echo $_SERVER["PHP_SELF"] . "?id=" . $texto["id"] . "&" . "acao=removetxt" ?>">Remover</a>
                <a class="btn btn-default btn-xs" href="edita_texto.php?id=<?php echo $texto["id"]; ?>">Editar</a>
              <?php endif; ?>
              </div>

              <hr>
              <div class="container" id="comments">
                <a name="form-anchor<?php echo $texto['id']; ?>comment"></a>
                <h2>Comentários</h2>
                
                <?php if (!empty($msg) && (str_contains($msg, $texto['id']))): ?>
                  <?= str_replace($texto['id'], "", $msg); ?>
                <?php endif; ?>

                <?php 
                  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
                  if (!$conn) {
                    die("Connection failed: " . mysqli_connect_error());
                  }

                  $textoID = $texto['id'];

                  $sql = "SELECT * FROM $table_comments WHERE artigoID = $textoID";
                  
                  $commentsID = mysqli_query($conn, $sql);
              
                  if (!$commentsID) {
                    die("Error: " . $sql . "<br>" . mysqli_error($conn));
                  }

                  mysqli_close($conn);
                ?>

                <?php if (mysqli_num_rows($commentsID) > 0): ?>
                  <?php while($comment = mysqli_fetch_assoc($commentsID)): ?>
                    <div class="comment" id="comment_<?= $comment['id'] ?>">
                      <h4>De: <?= $comment['nome'] ?></h4>
                      <p><?= $comment['comentario'] ?></p>
                      <?php if($login): ?>
                        <a class="btn btn-default btn-xs" href="<?php echo $_SERVER["PHP_SELF"] . "?id=" . $comment["id"] . "&" . "acao=remove" ?>">Remover</a>		
                        <a class="btn btn-default btn-xs" href="edita_comentario.php?id=<?php echo $comment["id"]; ?>">Editar</a>
                      <?php endif; ?>
                    </div>
                  <?php endwhile; ?>
                <?php else: ?>
                  Nenhum comentário enviado.
                <?php endif; ?>

                <hr>
                <h3>Novo comentário</h3>
                <form method="post" action="<?= sanitize($_SERVER['PHP_SELF']) ?>#form-anchor<?php echo $texto['id']; ?>comment">
                  <input type="hidden" name="form_artigoID" value="<?php echo $texto["id"] ?>">
                  <div class="form-group">
                    <label class="lab" for="form_name">Nome:</label><br>
                    <input type="text" name="form_name" value="<?= $form_name ?>" placeholder="Seu nome"><br>
                  </div>
                  <div class="form-group">
                    <label class="lab" for="form_comment">Comentário:</label><br>
                    <textarea name="form_comment" rows="8" cols="80" placeholder="Seu comentário"><?= $form_comment ?></textarea><br>
                  </div>
                  <input class="btn btn-default" type="submit" name="submit" value="Enviar">
                </form>
              </div>
            </div>  
          <?php endwhile; ?>
        <?php else: ?>
          Nenhum artigo publicado.
        <?php endif; ?>
          
        <div class="container">
          <div id=botaofinal>
            <?php if ($login): ?>
              <hr>
              <a href="cria_texto.php" class="btn btn-primary btn-block" role="button">Criar novo artigo!</a>
            <?php endif; ?>
          </div>
        </div>
 
      </div>
		</div>
	</body>
</html>