<?php
require "db_functions.php";
require "authenticate.php";
include "sanitize.php";

$error = false;
$password = $email = "";

if (!$login && $_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["email"]) && isset($_POST["password"])) {

    $conn = connect_db();

    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $email = sanitize($email);
    $password = sanitize($password);
    $password = md5($password);

    $sql = "SELECT id,name,email,password FROM $table_users
            WHERE email = '$email';";

    $result = mysqli_query($conn, $sql);
    if($result){
      if (mysqli_num_rows($result) > 0) {
        $user = mysqli_fetch_assoc($result);

        if ($user["password"] == $password) {

          $_SESSION["user_id"] = $user["id"];
          $_SESSION["user_name"] = $user["name"];
          $_SESSION["user_email"] = $user["email"];

          header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php");
          exit();
        }
        else {
          $error_msg = "Usuário ou Senha incorreta!";
          $error = true;
        }
      }
      else{
        $error_msg = "Usuário ou Senha incorreta!";
        $error = true;
      }
    }
    else {
      $error_msg = mysqli_error($conn);
      $error = true;
    }
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <title>Login - Blog do Senji</title>
</head>
<body>
  <div class="container">
    <h1>Login</h1><br>

    <?php if ($login): ?>
        <h3>Você já está logado!</h3>
        <a class="btn btn-default" href="index.php">Voltar</a>
      </body>
      </html>
      <?php exit(); ?>
    <?php endif; ?>

    <?php if ($error): ?>
      <h3 style="color:red;"><?php echo $error_msg; ?></h3>
    <?php endif; ?>

    <form action="login.php" method="post" class="form-horizontal">
      <div class="form-group">
        <label class="control-label col-sm-1" for="email">Email:</label>
        <div class="col-sm-5">  
          <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" required><br>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-1" for="password">Senha:</label>
        <div class="col-sm-5">
          <input type="password" class="form-control" name="password" value="" required><br>
        </div>
      </div>
      <input class="btn btn-default" type="submit" name="submit" value="Entrar">
      <a class="btn btn-default" href="index.php">Voltar</a>

    </form>
  </div>  
</body>
</html>
